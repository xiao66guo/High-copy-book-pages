//
//  AppDelegate.h
//  翻书效果
//
//  Created by 小果 on 16/7/23.
//  Copyright © 2016年 小果. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIPageViewControllerDataSource>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UIPageViewController *pageVC;

@end

