//
//  main.m
//  翻书效果
//
//  Created by 小果 on 16/7/23.
//  Copyright © 2016年 小果. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
